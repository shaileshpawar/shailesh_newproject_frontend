import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './home/main-page/main-page.component';
import { LoginComponent } from './customer/login/login.component';
import { SignupComponent } from './customer/signup/signup.component';
import { CartDetailsComponent } from './header/cart-details/cart-details.component';


const routes: Routes = [
 
  
  {path:'home' , component : MainPageComponent},
  {path:'signup' , component : SignupComponent},
  {path:'login' , component : LoginComponent},
  {path:'cart' , component : CartDetailsComponent},
  {path:'', component : MainPageComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
