import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/models/Product';
import { CartService } from 'src/app/shared/services/cart/cart.service';
import { Cart } from 'src/app/models/Cart';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input('product')//input coming from parent store component
  product: Product;

  productId: number;
  productName: string;
  customerId: number = 1;

  cartObj: Cart;

  constructor(private cartService: CartService) { }

  ngOnInit(): void {
  }




  addToCart(product: Product) {

    this.cartObj = {
      productId: product.productId,
      productName: product.productName,
      customerId: parseInt(localStorage.getItem('userId'))
    }
    console.log(this.cartObj);

    this.cartService.postProductToCart(this.cartObj).subscribe(
      {
        next: (result) => {
          console.log(result);
          this.cartService.fetchCartDetails().subscribe(
            {
              next: (result) => {
                this.cartService.cartDetailsArray = result;
                this.cartService.cartdetailArrayLength = this.cartService.cartDetailsArray.length;
              }
            }
          )
        }
      });

  }




}
