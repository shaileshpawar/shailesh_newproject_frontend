import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/Category';
import { CategoryService } from 'src/app/shared/services/category/category.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  categoriesArrayList : Category[] = [];

  constructor(private categoryService : CategoryService,private router : Router) { }

  ngOnInit(){

    this.collectAllcategories();
  }

  collectAllcategories(){
    this.categoryService.getAllCategories().subscribe(
      data => this.categoriesArrayList = data);
   
    
  }

  onSelectedCategory(categoryID : number){

      console.log(categoryID);

      this.router.navigate([''],//to navigate we used router,in navigate pass url and params querystring name value pair
      {
        queryParams :{
         'category' : categoryID
        }
      })
      
  }




}
