import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/shared/services/product/product.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Product } from 'src/app/models/Product';
import { AuthenticationService } from 'src/app/shared/services/auth/authentication.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {

  constructor(public productService: ProductService,
    private router: Router, private route: ActivatedRoute,
    private auth: AuthenticationService) { }

  productsArrayList: Product[] = [];
  searchStringFilter: string;
  categoryID: string;
  searchCategoryID: string;


  ngOnInit() {

    this.route.queryParamMap.subscribe({
      next: (paramMap: ParamMap) => {
        this.searchStringFilter = paramMap.get('searchby');
      }
    })
    this.route.queryParamMap.subscribe({
      next: (paramMap: ParamMap) => {
        this.searchCategoryID = paramMap.get('category');
      }
    })


    this.collectAllProducts();
  }



  collectAllProducts() {

    this.productService.fetchAllApprovedProducts().subscribe(
      data => this.productsArrayList = data);
    console.log(this.productsArrayList);


  }





}
