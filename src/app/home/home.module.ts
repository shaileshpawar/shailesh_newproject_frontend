import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page/main-page.component';
import { StoreComponent } from 'src/app/home/store/store.component';
import { ProductCardComponent } from 'src/app/home/product-card/product-card.component';
import { FilterComponent } from './filter/filter.component';
import { ProductSearchBarPipe } from 'src/app/shared/pipes/product-search-bar/product-search-bar.pipe';
import { CategoryFilterPipe } from 'src/app/shared/pipes/category-filter/category-filter.pipe';

//import { HeaderModule } from '../header/header.module';


@NgModule({
  declarations: [MainPageComponent,StoreComponent,ProductCardComponent,
    FilterComponent,ProductSearchBarPipe,CategoryFilterPipe, ],

  imports: [CommonModule,
    ],

  exports : [MainPageComponent,StoreComponent,ProductCardComponent,FilterComponent, ]
})
export class HomeModule { }
