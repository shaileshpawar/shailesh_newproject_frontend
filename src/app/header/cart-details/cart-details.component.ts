import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/shared/services/cart/cart.service';


@Component({
  selector: 'app-cart-details',
  templateUrl: './cart-details.component.html',
  styleUrls: ['./cart-details.component.scss']
})
export class CartDetailsComponent implements OnInit {

  

  constructor(public cartService : CartService) { 
   
    
  }

  ngOnInit(): void {
    this.cartService.fetchCartDetails().subscribe(
    {
      next: (result) =>{
         this.cartService.cartDetailsArray = result;
         this.cartService.cartdetailArrayLength = this.cartService.cartDetailsArray.length;
      }

    });
  }


 
}
