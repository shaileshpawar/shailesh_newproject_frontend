import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ProductService } from 'src/app/shared/services/product/product.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/services/auth/authentication.service';
import { CartService } from 'src/app/shared/services/cart/cart.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {


  searchString: string;
  isLogin: boolean = false;
  userName: string = "";
  userRole: string = "";
  navVar: any;
  navBarCheck : boolean = true;



  constructor(private productService: ProductService, private router: Router,
    public authService: AuthenticationService, private route: ActivatedRoute,
    public cartService: CartService) { }


  ngOnInit(): void {

    this.userName = localStorage.getItem('userName');
    this.userRole = localStorage.getItem('userRole');



    this.authService.currentData.subscribe(m => {
      if (m == 1) {
        this.userName = localStorage.getItem('userName');
        this.userRole = localStorage.getItem('userRole');
        console.log("name=" + this.userName);
        console.log("role=" + this.userRole);


        this.cartService.fetchCartDetails().subscribe(
          {
            next: (result) => {
              this.cartService.cartDetailsArray = result;
              this.cartService.cartdetailArrayLength = this.cartService.cartDetailsArray.length;
            }
          })

      }
    })



    this.cartService.fetchCartDetails().subscribe(
      {
        next: (result) => {
          this.cartService.cartDetailsArray = result;
          this.cartService.cartdetailArrayLength = this.cartService.cartDetailsArray.length;
        }
      })



  }

  searchStringSubmit() {
    this.router.navigate([''],//to navigate we used router,in navigate pass url and params querystring name value pair
      {
        queryParams: {
          'searchby': this.searchString
        }
      })
    //this.searchString = null;
  }


  logout() {
    this.authService.removeToken();
    localStorage.clear();
    this.ngOnInit();
    this.router.navigate(['']);
  }













}
