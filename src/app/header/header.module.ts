import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CartDetailsComponent } from './cart-details/cart-details.component';
@NgModule({
  declarations: [NavbarComponent, CartDetailsComponent],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    FormsModule
    
  ],
  exports : [NavbarComponent]
})
export class HeaderModule { }
