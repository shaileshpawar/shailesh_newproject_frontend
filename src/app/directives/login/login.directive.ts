import { Directive, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Directive({
  selector: '[appLogin]'
})
export class LoginDirective {

  @Output() setForm: EventEmitter<FormGroup> = new EventEmitter();
  @Input() formElementslogin: string[] = [];
  constructor() {
  }

  ngOnInit() {
    let loginForm = new FormGroup({});

    this.formElementslogin.forEach((element) => {
      loginForm.addControl(element, new FormControl())
    })

    this.setForm.emit(loginForm);
  }

}
