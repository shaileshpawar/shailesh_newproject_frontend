import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {

  constructor(public auth : AuthenticationService,private route : Router ){}

  canActivate(): boolean  {

    if (this.auth.isAuthenticated())
    {
      return true;
      
    }
    console.log("You are not authorized !!!");
      this.route.navigate(['']);
      return false;
  }


}
