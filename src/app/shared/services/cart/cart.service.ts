import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Cart } from 'src/app/models/Cart';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../auth/authentication.service';
@Injectable({
  providedIn: 'root'
})
export class CartService {

  postProductToCartUrl: string = 'http://localhost:61205/api/cart/postCart';
  getCartDetailsByCustomerIdUrl: string = 'http://localhost:61205/api/cart/getCartDetailsByCustomerId';
  cartObj: Cart;
  public cartDetailsArray: Cart[] = [];
  public cartdetailArrayLength: number;


  constructor(private http: HttpClient, private auth: AuthenticationService) {

  }


  postProductToCart(cartObj: Cart) {
    return this.http.post(this.postProductToCartUrl, cartObj,
      {
        headers: {
          'authorization': this.auth.getToken()
        }
      });
  }

  fetchCartDetails() {

    let userId = localStorage.getItem('userId');
    let customerId = parseInt(userId);


    return this.http.get<Cart[]>(`${this.getCartDetailsByCustomerIdUrl}/${customerId}`,
      {
        headers: {
          'authorization': this.auth.getToken()
        }
      });
  }







}
