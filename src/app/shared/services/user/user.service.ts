import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  postUserUrl: string = 'http://localhost:61205/api/customers/addCustomer';


  constructor(private http: HttpClient) { }

  signup(userData: any) {
    console.log(userData);
    return this.http.post(this.postUserUrl, userData);
  }







}



