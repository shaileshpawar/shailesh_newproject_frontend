import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from 'src/app/models/Category';
import { map } from 'rxjs/Operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {


  getAllCategoriesUrl: string = 'http://localhost:61205/api/categories/getCategories';
  public categoriesList: Category[] = [];

  constructor(private http: HttpClient) {
    this.fetchCategoriesList();
  }

  getAllCategories(): Observable<Category[]> {

    return this.http.get<Category[]>(this.getAllCategoriesUrl);

  }

  fetchCategoriesList() {

    this.http.get<Category[]>(this.getAllCategoriesUrl).subscribe(data => this.categoriesList = data);
  }





}
