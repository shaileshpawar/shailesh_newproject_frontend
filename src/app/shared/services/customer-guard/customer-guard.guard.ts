import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerGuardGuard implements CanActivate {

  constructor(public auth : AuthenticationService,private route : Router ){}

  canActivate(): boolean  {

    if (localStorage.getItem('userRole') == "Customer")
    {
      return true;
      
    }
    console.log("You are not authorized !!!");
      this.route.navigate(['admin']);
      return false;
  }
  
}
