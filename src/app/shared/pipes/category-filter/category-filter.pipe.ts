import { Pipe, PipeTransform } from '@angular/core';
import { Product } from 'src/app/models/Product';

@Pipe({
  name: 'categoryFilter'
})
export class CategoryFilterPipe implements PipeTransform {
  transform(products : Product[],searchCategoryID : string) : Product []{
    if(!products || !searchCategoryID ){
      return products;
    }
    var id = parseInt(searchCategoryID);
    return products.filter((products) => products.categoryId == id);
}
}
