import { Pipe, PipeTransform } from '@angular/core';
import { Product } from 'src/app/models/Product';

@Pipe({
  name: 'productSearchBar'
})
export class ProductSearchBarPipe implements PipeTransform {

  transform(products : Product[],searchStringFilter : string) : Product []{
    
   

    if(!products || !searchStringFilter ){

      return products;
    }
   

    return products.filter((products) => products.productName.includes(searchStringFilter));


    
  }

}
