import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { LoginDirective } from '../directives/login/login.directive';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SignupComponent } from './signup/signup.component';
import { SignupDirective } from '../directives/signup/signup.directive';





@NgModule({
  declarations: [LoginComponent,LoginDirective, SignupDirective,SignupComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports : [LoginComponent,SignupComponent]
})
export class CustomerModule { }
