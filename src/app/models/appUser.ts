export interface appUser{
    email : string;
    password : string;
    isAdmin : boolean;
}