import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomerModule } from './customer/customer.module';
import { FooterbarModule } from './footerbar/footerbar.module';
import { HomeModule } from './home/home.module';
import { ProductService } from 'src/app/shared/services/product/product.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoryService } from 'src/app/shared/services/category/category.service';
import { UserService } from 'src/app/shared/services/user/user.service';
import { AuthenticationService } from 'src/app/shared/services/auth/authentication.service';
import { AdminModule } from 'src/app/admin/admin.module';
import { AuthGuardGuard } from 'src/app/shared/services/auth-guard/auth-guard.guard';
import { CustomerGuardGuard } from 'src/app/shared/services/customer-guard/customer-guard.guard';
import { HeaderModule } from './header/header.module';
import { CartService } from './shared/services/cart/cart.service';
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HeaderModule,
    CustomerModule,
    FooterbarModule,
    HomeModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AdminModule,
    AppRoutingModule
    
    
  ],
  providers: [ProductService,CategoryService,UserService,AuthenticationService,AuthGuardGuard,CartService,CustomerGuardGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
