import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { ProductListComponent } from './product-list/product-list.component';
import { AdminComponent } from './admin/admin.component';
import { AdminRoutingModule } from 'src/app/admin/admin-routing.module';
import { UpdateProductComponent } from './product-list/update-product/update-product.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';






@NgModule({
  declarations: [CustomerListComponent,ProductListComponent,AdminComponent, UpdateProductComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule
  ],
  exports : [CustomerListComponent,ProductListComponent,AdminComponent,]
})
export class AdminModule { }
