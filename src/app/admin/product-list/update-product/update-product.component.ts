import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from 'src/app/models/Product';
import { ProductService } from 'src/app/shared/services/product/product.service';
import { CategoryService } from 'src/app/shared/services/category/category.service';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.scss']
})
export class UpdateProductComponent implements OnInit {

  @Input() product:Product;
  productobj : Product;
  categoryIdInput : string ="";
  @Output() closeModalInput : EventEmitter<boolean> = new EventEmitter();

  constructor(public productService : ProductService,
              public categoryService : CategoryService ) { }

  ngOnInit(): void {
      this.loadCategoriesList();
   
  }

  loadCategoriesList(){
    this.categoryService.fetchCategoriesList();
  }



  addProduct(obj : Product){
    let categoryId = this.categoryIdInput.toString();
    let catId = categoryId.slice(0,1);
    obj.categoryId = parseInt(catId);
    // this.productService.postProduct(obj).subscribe(
    //   {
    //     next : (result)=>{
    //        console.log(result);
    //     }
    //   });
    this.productService.fetchProductList();
    this.closeModalInput.emit(false);
   
  }


  updateProduct(obj : Product){
    let categoryId = this.categoryIdInput.toString();
    let catId = categoryId.slice(0,1);
    obj.categoryId = parseInt(catId);
    this.closeModalInput.emit(false);
  }






}
